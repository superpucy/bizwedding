
var _menus = [];
function getMenus(callback)
{
	_menus = [];
    getDb().collection("menu").where("disabled", "==", false).get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
			_menus.push({id:doc.id, data:doc.data()});
        });
		callback(_menus);
    });
}
