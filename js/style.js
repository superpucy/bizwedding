

function getStyles(id,callback)
{
	var styles = [];
    getDb().collection("package").where("style", "array-contains", id).get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
			styles.push({
				id:doc.id,
				data:doc.data()
			});
        });
		callback(styles);
    });
}

var _styles = [];
function getStyle(callback)
{
	
    getDb().collection("styles").where("disabled", "==", false).get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
			_styles.push({id:doc.id, data:doc.data()});
        });
		callback(_styles);
    });
}

function getStyleName(id,callback)
{
	db.collection("styles").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            if(doc.id == id){
                callback(doc.data().title);
            }
        });
    });
}