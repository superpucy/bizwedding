

function getPackage(id,callback)
{
	var styles = [];
    getDb().collection("package").doc(id).get().then((doc) => {
		callback({
				id:doc.id,
				data:doc.data()
			});
    });
}

function getDetails(id,type,callback)
{
	var details = [];
    getDb().collection("/package/"+id+"/"+type).get().then((querySnapshot) => {
		querySnapshot.forEach((doc) => {
			details.push({
				id:doc.id,
				data:doc.data()
			});
        });
		callback(details);
    });
}
